package main

import (
	"github.com/labstack/echo"
	"github.com/labstack/echo/engine/standard"
	"github.com/labstack/echo/middleware"

	"github.com/go-xorm/xorm"
	_ "github.com/mattn/go-sqlite3"

	"html/template"
	"io"
	"log"
	"net/http"
	"time"
)

type User struct {
	ID      int64     `xorm:"id"`
	Agent   string    `xorm:"agent"`
	Conn    string    `xorm:"conn"`
	Created time.Time `xorm:"created"`
}

type Template struct {
	templates *template.Template
}

func (t *Template) Render(w io.Writer, name string, data interface{}, c echo.Context) error {
	return t.templates.ExecuteTemplate(w, name, data)
}

var engine *xorm.Engine

func main() {
	t := &Template{
		templates: template.Must(template.ParseGlob("templates/*.html")),
	}

	e := echo.New()
	e.Use(middleware.Logger())
	e.Use(middleware.Recover())

	e.Use(middleware.StaticWithConfig(middleware.StaticConfig{
		Root:   "public",
		Browse: false,
	}))

	e.SetRenderer(t)

	engine, err := xorm.NewEngine("sqlite3", "./db.sqlite")

	if err != nil {
		panic(err)
	}

	engine.Sync2(new(User))

	e.Get("/82c62f254b4189d13677061fe3a990cf", func(c echo.Context) error {

		r := c.Request().RemoteAddress()
		a := c.Request().UserAgent()

		_, er := engine.Insert(&User{
			Agent:   a,
			Conn:    r,
			Created: time.Now(),
		})

		if er != nil {
			log.Print(er)
		}
		return c.Render(http.StatusOK, "index", "Pic of the World")
	})

	e.Get("/dboard", func(c echo.Context) error {
		u := []User{}
		err := engine.Find(&u)

		if err != nil {
			panic(err)
		}

		return c.Render(http.StatusOK, "dashboard", u)
	})

	e.Run(standard.New(":1323"))
}
